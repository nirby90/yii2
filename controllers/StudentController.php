<?p 
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;


class StudentController extends Controller
{
   public function actionView($id)
  
    {
		$student = Student::getName($id);
        return $this->render('view',['student' => $student]);
	} 
	public function actionShow()
  
    {
		$student = Student::getStudent();
        return $this->render('show',['student' => $student]);
	} 
}
?p>