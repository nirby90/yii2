<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movies`.
 */
class m170612_090610_create_movies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('movies', [
            'id' => $this->primaryKey(),
            'movie_name' => $this->string(),
            'genre' => $this->string(),
            'min_age' => $this->string(),
            'movie_grade' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movies');
    }
}
