<?php

use yii\db\Migration;

/**
 * Handles the creation of table `students`.
 */
class m170529_084202_create_students_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('students', [
             'id' => $this->primaryKey(),
			 'idNumber' => $this->integer()->notNull(),
			 'name' => $this->string()->notNull(),
			 'age' => $this->integer()->notNull(),
			    ]);
    }
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('students');
        echo "m170529_084202_create_students_table cannot be reverted.\n";

        return false;
    }
}
