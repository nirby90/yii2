<?php

namespace app\models;

use yii\db\ActiveRecord;

class Student extends ActiveRecord
{
	
	public static function tableName(){
		return 'students';		
	}
	
	
     public static function getName($id){
		 
		$student = self::findOne($id); 
		 
		return $student;
	 }
	 
	 public static function getStudent(){
		 
		$student = self::find()->all(); 
		 
		return $student;
	 }

}
