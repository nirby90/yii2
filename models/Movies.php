<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movies".
 *
 * @property integer $id
 * @property string $movie_name
 * @property string $genre
 * @property string $min_age
 * @property string $movie_grade
 */
class Movies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['movie_name', 'genre', 'min_age', 'movie_grade'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'movie_name' => 'Movie Name',
            'genre' => 'Genre',
            'min_age' => 'Min Age',
            'movie_grade' => 'Movie Grade',
        ];
    }
}
