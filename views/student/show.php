<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'View students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
	<?php foreach ($student as $value): ?>
	    <p>
	    	<b>Name:</b> <?= $value->name ?>
	    	<br />
	    	<b>ID:</b><?= $value->idNumber ?>
	    	<br />
	    	<b>Age:</b><?= $value->age ?>
	    	<br />    	
	    </p>
    <?php endforeach; ?>
    <code><?= __FILE__ ?></code>
</div>
