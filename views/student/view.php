<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'View students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
	<b>Name:</b>  <?php echo $student->name ?>
	<br />
	<b>ID:</b> <?php echo $student->idNumber ?>
	<br />
	<b>Age:</b> <?php echo $student->age ?>
	<br />
    <code><?= __FILE__ ?></code>
</div>
